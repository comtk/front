import { fileURLToPath } from 'url';
import vue from '@vitejs/plugin-vue';
import vueJsx from '@vitejs/plugin-vue-jsx';
import AutoImport from 'unplugin-auto-import/vite';
import Components from 'unplugin-vue-components/vite';
import DefineOptions from 'unplugin-vue-define-options/vite';
import { defineConfig, loadEnv } from 'vite';
import Pages from 'vite-plugin-pages';
import Layouts from 'vite-plugin-vue-layouts';
import vuetify from 'vite-plugin-vuetify';

export default defineConfig(({ command, mode, ssrBuild }) => {
	const env = loadEnv(mode, process.cwd());

	return {
		plugins: [
			vue(),
			vueJsx(),
			vuetify({
				styles: {
					configFile: 'src/assets/pro/scss/_vuetify.scss',
				},
			}),
			Pages({}),
			Layouts({
				layoutsDirs: 'src/layouts',
				defaultLayout: 'default'
			}),
			Components({
				dirs: ['src/components'],
				dts: true,
			}),
			AutoImport({
				imports: ['vue', 'vue-router', '@vueuse/core', 'vue-i18n', 'pinia'],
				vueTemplate: true,
			}),
			DefineOptions(),
		],
		define: {'process.env': {}},
		resolve: {
			alias: {
				'@': fileURLToPath(new URL('./src', import.meta.url)),
				'@assets': fileURLToPath(new URL('./src/assets', import.meta.url)),
				'@axios': fileURLToPath(new URL('./src/plugins/axios', import.meta.url)),
				'@utils': fileURLToPath(new URL('./src/utils/index', import.meta.url)),
				'@configured-variables': fileURLToPath(new URL('./src/assets/pro/scss/_template.scss', import.meta.url),),
			},
		},
		build: {chunkSizeWarningLimit: 5000,},
		esbuild: {drop: ['console', 'debugger'],},
		optimizeDeps: {exclude: ['vuetify'], entries: ['./src/**/*.vue'],},
	};
});
