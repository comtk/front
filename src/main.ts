/* eslint-disable import/order */
import '@//plugins/iconify/icons-bundle';
import App from '@/App.vue';
import vuetify from '@/plugins/vuetify';
import router from '@/router';
import { createPinia } from 'pinia';
import { createApp } from 'vue';
import userApiClient from '@/plugins/user-axios';

const app = createApp(App);
app.use(vuetify);
app.use(createPinia());
app.use(router);
app.provide('userApiClient', userApiClient);
app.mount('#app');
