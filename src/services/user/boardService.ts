import proApiClient from '@/plugins/user-axios';
import type { ResponseInterface, SearchInterface } from '@/interface/user/common';
import type { BoardInterface } from '@/interface/user/board';

export const apiGetBoardArticles = async (
	boardId: number,
	searchData: SearchInterface,
): Promise<ResponseInterface> => {
	return await proApiClient.get(`/user/api/v1/board/${boardId}`, {
		params: { ...searchData },
	});
};

export const apiGetBoardArticleDetail = async (
	boardId: number | string,
	articleId: number | string,
): Promise<ResponseInterface> => {
	return await proApiClient.get(`/user/api/v1/board/${boardId}/${articleId}`);
};

export const apiGetBoardArticlePrev = async (
	boardId: number | string,
	articleId: number | string,
): Promise<ResponseInterface> => {
	return await proApiClient.get(`/user/api/v1/board/${boardId}/prev/${articleId}`);
};

export const apiGetBoardArticleNext = async (
	boardId: number | string,
	articleId: number | string,
): Promise<ResponseInterface> => {
	return await proApiClient.get(`/user/api/v1/board/${boardId}/next/${articleId}`);
};
