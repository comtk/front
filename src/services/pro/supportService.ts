import proApiClient from '@/plugins/pro-axios';
import type {SearchInterface} from '@/interface/pro/common';
import type {SupportInterface} from '@/interface/pro/support';

export const apiSaveSupport = async (into: SupportInterface) => {
  return await proApiClient.post('/pro/api/v1/support', {...into});
};

export const apiUpdateSupport = async (info: SupportInterface) => {
    return await proApiClient.put(`/pro/api/v1/support`, {...info});
};

export const apiGetSupport = async () => {
    return await proApiClient.get('/pro/api/v1/support');
};

export const apiDeleteSupport = async (id: number) => {
    return await proApiClient.delete(`/pro/api/v1/support/${id}`);
};

export const apiGetSupportInfo = async (id: number): Promise<SupportInterface> => {
    return await proApiClient.get(`/pro/api/v1/support/${id}`);
};

export const apiGetSearchSupportInfo = async (searchData: SearchInterface) => {
    return await proApiClient.get(`/pro/api/v1/support/list`, {
        params: {...searchData},
    });
};
