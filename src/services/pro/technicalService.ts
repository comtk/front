import proApiClient from '@/plugins/pro-axios';
import type {SearchInterface} from '@/interface/pro/common';
import type {TechnicalInterface} from '@/interface/pro/technical';

export const apiSaveTechnical = async (into: TechnicalInterface) => {
  return await proApiClient.post('/pro/api/v1/technical', {...into});
};

export const apiUpdateTechnical = async (info: TechnicalInterface) => {
    return await proApiClient.put(`/pro/api/v1/technical`, {...info});
};

export const apiGetTechnical = async () => {
    return await proApiClient.get('/pro/api/v1/technical');
};

export const apiDeleteTechnical = async (id: number) => {
    return await proApiClient.delete(`/pro/api/v1/technical/${id}`);
};

export const apiGetTechnicalInfo = async (id: number): Promise<TechnicalInterface> => {
    return await proApiClient.get(`/pro/api/v1/technical/${id}`);
};

export const apiGetSearchTechnicalInfo = async (searchData: SearchInterface) => {
    return await proApiClient.get(`/pro/api/v1/technical/list`, {
        params: {...searchData},
    });
};
