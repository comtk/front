import proApiClient from '@/plugins/pro-axios';
import type {SearchInterface} from '@/interface/pro/common';
import type {BannerInterface} from '@/interface/pro/banner';

export const apiGetEnumType = async (type: string) => {
    return await proApiClient.get('/pro/api/v1/enum/' + type);
};

export const apiGetEnumAll = async () => {
  return await proApiClient.get('/pro/api/v1/enum');
};

