import proApiClient from '@/plugins/pro-axios';
import type {SearchInterface} from '@/interface/pro/common';
import type {MemberInterface} from '@/interface/pro/member';

export const apiSaveMember = async (into: MemberInterface) => {
  return await proApiClient.post('/pro/api/v1/member', {...into});
};

export const apiUpdateMember = async (info: MemberInterface) => {
    return await proApiClient.put(`/pro/api/v1/member`, {...info});
};

export const apiGetMember = async () => {
    return await proApiClient.get('/pro/api/v1/member');
};

export const apiDeleteMember = async (id: number) => {
    return await proApiClient.delete(`/pro/api/v1/member/${id}`);
};

export const apiGetMemberInfo = async (id: number): Promise<MemberInterface> => {
    return await proApiClient.get(`/pro/api/v1/member/${id}`);
};

export const apiGetSearchMemberInfo = async (searchData: SearchInterface) => {
    return await proApiClient.get(`/pro/api/v1/member/list`, {
        params: {...searchData},
    });
};

export const apiGetMemberSimpleList = async () => {
    return await proApiClient.get(`/pro/api/v1/member/simple/list`);
}

export const apiPutMemberInitPassword = async (id: number) => {
  return await proApiClient.put(`/pro/api/v1/member/${id}`);
}
