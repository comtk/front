import proApiClient from '@/plugins/pro-axios';
import type {BoardsInterface} from '@/interface/pro/boards';
import type {SearchInterface} from '@/interface/pro/common';
import {ArticlesInterface} from '@/interface/pro/articles';

export const apiSaveBoards = async (boardsInfo: BoardsInterface) => {
    return await proApiClient.post('/pro/api/v1/boards', {...boardsInfo});
};

export const apiUpdateBoards = async (boardsInfo: BoardsInterface) => {
    return await proApiClient.put(`/pro/api/v1/boards/${boardsInfo.id}`, {...boardsInfo});
};

export const apiGetBoards = async () => {
    return await proApiClient.get('/pro/api/v1/boards');
};

export const apiDeleteBoards = async (boardsId: number) => {
  return await proApiClient.delete(`/pro/api/v1/boards/${boardsId}`);
};

export const apiGetBoardsInfo = async (boardsId: number) => {
    return await proApiClient.get(`/pro/api/v1/boards/${boardsId}`);
};

export const apiGetSearchBoardsInfo = async (searchData: SearchInterface) => {
    return await proApiClient.get(`/pro/api/v1/boards`, {
        params: {...searchData},
    });
};
