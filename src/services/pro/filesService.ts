import proApiClient from '@/plugins/pro-axios';

export const apiSaveFiles = async (files: any) => {
    return await proApiClient.post(`/pro/api/v1/files/upload`, {...files}, {headers: {'Content-Type': 'multipart/form-data'}});
};

export const apiDeleteFiles = async (id: number) => {
    return await proApiClient.delete(`/pro/api/v1/files/${id}`);
};
