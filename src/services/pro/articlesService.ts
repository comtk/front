import type {ServerOptions} from 'vue3-easy-data-table';
import proApiClient from '@/plugins/pro-axios';
import type {ArticlesFetchInterface, ArticlesInterface, ArticlesServerOptionsInterface} from '@/interface/pro/articles';
import type {SearchArticlesInterface, SearchInterface} from '@/interface/pro/common';


export const apiSaveArticles = async (articlesInfo: ArticlesInterface) => {
    return await proApiClient.post(`/pro/api/v1/article/${articlesInfo.boardsId}`, {...articlesInfo});
};

export const apiUpdateArticles = async (articlesInfo: ArticlesInterface) => {
    return await proApiClient.put(`/pro/api/v1/article/${articlesInfo.boardsId}/${articlesInfo.id}`, {...articlesInfo});
};

export const apiGetArticles = async (serverOptions: ArticlesServerOptionsInterface) => {
    return await proApiClient.get(`/pro/api/v1/article/${serverOptions.boardsId}`, {
        params: {
            start: (serverOptions.page - 1) * 10,
            length: serverOptions.rowsPerPage,
        },
    });
};

export const apiDeleteArticle = async (articlesInfo: ArticlesInterface) => {
  return await proApiClient.delete(`/pro/api/v1/article/${articlesInfo.boardsId}/${articlesInfo.id}`);
};

export const apiGetArticlesInfo = async (articleParams: ArticlesFetchInterface) => {
    return await proApiClient.get(`/pro/api/v1/article/${articleParams.boardsId}/${articleParams.articlesId}`);
};

export const apiGetSearchArticlesInfo = async (searchData: SearchArticlesInterface) => {
    return await proApiClient.get(`/pro/api/v1/article/${searchData.boardsId}`, {
        params: {...searchData},
    });
};
