import proApiClient from '@/plugins/pro-axios';
import type {SearchInterface} from '@/interface/pro/common';
import type {DeviceInterface} from '@/interface/pro/device';

export const apiSaveDevice = async (into: DeviceInterface) => {
    return await proApiClient.post('/pro/api/v1/device', {...into});
};

export const apiUpdateDevice = async (info: DeviceInterface) => {
    return await proApiClient.put(`/pro/api/v1/device`, {...info});
};

export const apiGetDevice = async () => {
    return await proApiClient.get('/pro/api/v1/device');
};

export const apiDeleteDevice = async (id: number) => {
    return await proApiClient.delete(`/pro/api/v1/device/${id}`);
};

export const apiGetDeviceInfo = async (id: number): Promise<DeviceInterface> => {
    return await proApiClient.get(`/pro/api/v1/device/${id}`);
};

export const apiGetSearchDeviceInfo = async (searchData: SearchInterface) => {
    return await proApiClient.get(`/pro/api/v1/device/list`, {
        params: {...searchData},
    });
};
