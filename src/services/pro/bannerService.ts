import proApiClient from '@/plugins/pro-axios';
import type {SearchInterface} from '@/interface/pro/common';
import type {BannerInterface} from '@/interface/pro/banner';

export const apiSaveBanner = async (bannerInfo: BannerInterface) => {
    return await proApiClient.post('/pro/api/v1/banner', {...bannerInfo});
};

export const apiUpdateBanner = async (bannerInfo: BannerInterface) => {
    return await proApiClient.put(`/pro/api/v1/banner/${bannerInfo.id}`, {...bannerInfo});
};

export const apiGetBanner = async () => {
    return await proApiClient.get('/pro/api/v1/banner');
};

export const apiDeleteBanner = async (bannerId: number) => {
    return await proApiClient.delete(`/pro/api/v1/banner/${bannerId}`);
};

export const apiGetBannerInfo = async (bannerId: number): Promise<BannerInterface> => {
    return await proApiClient.get(`/pro/api/v1/banner/${bannerId}`);
};

export const apiGetSearchBannerInfo = async (searchData: SearchInterface) => {
    return await proApiClient.get(`/pro/api/v1/banner`, {
        params: {...searchData},
    });
};
