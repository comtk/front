import proApiClient from '@/plugins/pro-axios';
import type {SearchInterface} from '@/interface/pro/common';
import type {InfraInterface} from '@/interface/pro/infra';

export const apiSaveInfra = async (into: InfraInterface) => {
  return await proApiClient.post('/pro/api/v1/infra', {...into});
};

export const apiUpdateInfra = async (info: InfraInterface) => {
    return await proApiClient.put(`/pro/api/v1/infra`, {...info});
};

export const apiGetInfra = async () => {
    return await proApiClient.get('/pro/api/v1/infra');
};

export const apiDeleteInfra = async (id: number) => {
    return await proApiClient.delete(`/pro/api/v1/infra/${id}`);
};

export const apiGetInfraInfo = async (id: number): Promise<InfraInterface> => {
    return await proApiClient.get(`/pro/api/v1/infra/${id}`);
};

export const apiGetSearchInfraInfo = async (searchData: SearchInterface) => {
    return await proApiClient.get(`/pro/api/v1/infra/list`, {
        params: {...searchData},
    });
};

export const apiGetInfraSimpleList = async () => {
    return await proApiClient.get(`/pro/api/v1/infra/simple/list`);
};
