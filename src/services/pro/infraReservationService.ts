import proApiClient from '@/plugins/pro-axios';
import type {SearchInterface} from '@/interface/pro/common';
import type {InfraReservationInterface} from '@/interface/pro/infraReservation';

export const apiSaveInfraReservation = async (into: InfraReservationInterface) => {
  return await proApiClient.post('/pro/api/v1/infra/reservation', {...into});
};

export const apiUpdateInfraReservation = async (info: InfraReservationInterface) => {
    return await proApiClient.put(`/pro/api/v1/infra/reservation`, {...info});
};

export const apiGetInfraReservation = async () => {
    return await proApiClient.get('/pro/api/v1/infra/reservation');
};

export const apiDeleteInfraReservation = async (id: number) => {
    return await proApiClient.delete(`/pro/api/v1/infra/reservation/${id}`);
};

export const apiGetInfraReservationInfo = async (id: number): Promise<InfraReservationInterface> => {
    return await proApiClient.get(`/pro/api/v1/infra/reservation/${id}`);
};

export const apiGetSearchInfraReservationInfo = async (searchData: SearchInterface) => {
    return await proApiClient.get(`/pro/api/v1/infra/reservation/list`, {
        params: {...searchData},
    });
};
