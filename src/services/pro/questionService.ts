import proApiClient from '@/plugins/pro-axios';
import type {SearchInterface} from '@/interface/pro/common';
import type {QuestionInterface} from '@/interface/pro/question';

export const apiSaveQuestion = async (into: QuestionInterface) => {
  return await proApiClient.post('/pro/api/v1/question', {...into});
};

export const apiUpdateQuestion = async (info: QuestionInterface) => {
    return await proApiClient.put(`/pro/api/v1/question`, {...info});
};

export const apiGetQuestion = async () => {
    return await proApiClient.get('/pro/api/v1/question');
};

export const apiDeleteQuestion = async (id: number) => {
    return await proApiClient.delete(`/pro/api/v1/question/${id}`);
};

export const apiGetQuestionInfo = async (id: number): Promise<QuestionInterface> => {
    return await proApiClient.get(`/pro/api/v1/question/${id}`);
};

export const apiGetSearchQuestionInfo = async (searchData: SearchInterface) => {
    return await proApiClient.get(`/pro/api/v1/question`, {
        params: {...searchData},
    });
};
