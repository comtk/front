import proApiClient from '@/plugins/pro-axios';
import {VisitConnectInterface, VisitMediaInterface} from '@/interface/pro/visit';
import {SearchDateInterface, SearchInterface} from '@/interface/pro/common';

export const apiChartConnection = async (search : SearchDateInterface) => {
    return await proApiClient.get('/pro/api/v1/main/chart/connection?startDate=' + search.startDate + '&endDate=' + search.endDate);
};

export const apiChartMedia = async (search : SearchDateInterface) => {
  return await proApiClient.get('/pro/api/v1/main/chart/media?startDate=' + search.startDate + '&endDate=' + search.endDate);
};


