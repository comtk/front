import proApiClient from '@/plugins/pro-axios';
import type {SearchInterface} from '@/interface/pro/common';
import type {SupportRegisterInterface} from '@/interface/pro/supportRegister';

export const apiSaveSupportRegister = async (into: SupportRegisterInterface) => {
  return await proApiClient.post('/pro/api/v1/support/register', {...into});
};

export const apiUpdateSupportRegister = async (info: SupportRegisterInterface) => {
    return await proApiClient.put(`/pro/api/v1/support/register`, {...info});
};

export const apiGetSupportRegister = async () => {
    return await proApiClient.get('/pro/api/v1/support/register');
};

export const apiDeleteSupportRegister = async (id: number) => {
    return await proApiClient.delete(`/pro/api/v1/support/register/${id}`);
};

export const apiGetSupportRegisterInfo = async (id: number): Promise<SupportRegisterInterface> => {
    return await proApiClient.get(`/pro/api/v1/support/register/${id}`);
};

export const apiGetSearchSupportRegisterInfo = async (searchData: SearchInterface) => {
    return await proApiClient.get(`/pro/api/v1/support/register/list`, {
        params: {...searchData},
    });
};
