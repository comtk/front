import type {UserLoginInterface} from '@/interface/pro/user';
import proApiClient from '@/plugins/pro-axios';
import {userStore} from '@/store/pro/user';

export const apiGetUserInfo = async () => {
    const store = userStore();
    const userInfo = await proApiClient.get('/pro/api/v1/my');
    store.handleCurrentUser(userInfo.data);
    const token = localStorage.getItem('token');
    const refreshToken = localStorage.getItem('refreshToken');
    if (token && refreshToken) {
        store.handleCurrentToken({token, refreshToken});
    }

    return userInfo;
};

export const apiLogin = async (accountInfo: UserLoginInterface) => {
    const response: any = await proApiClient.post('/pro/api/auth/login', {
        username: accountInfo.username,
        password: accountInfo.password,
    });

    const store = userStore();
    if (response) {
        const {token, refreshToken} = response.data;
        store.handleCurrentToken(response.data);
        if (accountInfo.remember) {
            localStorage.setItem('token', token);
            localStorage.setItem('refreshToken', refreshToken);
            localStorage.setItem('remember', accountInfo.remember.toString());
        }
        await apiGetUserInfo();
    } else {
        store.handleCurrentUser(undefined);
        store.handleCurrentToken(undefined);

        return Promise.reject(new Error('invalid credentials'));
    }
};

export const apiLogout = () => {
    const store = userStore();
    localStorage.removeItem('token');
    localStorage.removeItem('refreshToken');
    localStorage.removeItem('remember');
    store.handleCurrentUser(undefined);
    store.handleCurrentToken(undefined);
};

export const apiAuthToken = async () => {
    const refreshToken = localStorage.getItem('refreshToken');
    const response: any = await proApiClient.post('/pro/api/auth/token');

    const store = userStore();
    if (response) {
        const {token, refreshToken} = response.data;
        store.handleCurrentToken(response.data);
        localStorage.setItem('token', token);
        localStorage.setItem('refreshToken', refreshToken);
        await apiGetUserInfo();
    } else {
        store.handleCurrentUser(undefined);
        store.handleCurrentToken(undefined);

        return Promise.reject(new Error('invalid credentials'));
    }
};
