import type { AxiosRequestConfig, AxiosResponse } from 'axios';
import axios from 'axios';
import router from '@/router';

const userApiClient = axios.create({
	baseURL: import.meta.env.VITE_API_BASE_URL,
	headers: {
		'Content-Type': 'application/json',
	},
});

export const setToken = (token: string): string => {
	return `Bearer ${token}`;
};

userApiClient.interceptors.request.use(
	(config: AxiosRequestConfig) => {
		console.log('axios.js request : ', config);
		const token = localStorage.getItem('token');
		if (token) {
			config.headers = {
				...config.headers,
				Authorization: setToken(token),
			};
		}

		return config;
	},
	(error: any) => {
		return Promise.reject(error);
	},
);

userApiClient.interceptors.response.use(
	(response: AxiosResponse) => {
		return response;
	},
	async (error: any) => {
		const { status, config, data } = error.response;
		const responseConfig = config;
		if (status === 400) {
			return Promise.resolve(data);
		}

		// Do something with response error
		return Promise.reject(error);
	},
);

export default userApiClient;
