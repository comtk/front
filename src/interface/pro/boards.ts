export interface BoardsInterface {
    id?: number;
    type?: string;
    name?: string;
    enableSecret?: boolean;
    enableReply?: boolean;
    enableComment?: boolean;
    enableWriteAnonymousUser?: boolean;
    enableEditor?: boolean;
    enableFileUpload?: boolean;
    uploadableNumberOfFiles?: number;
    enable?: boolean;
}

export const initBoards = {
    id: 0,
    type: 'POST',
    name: '',
    enableSecret: false,
    enableReply: false,
    enableComment: false,
    enableWriteAnonymousUser: false,
    enableEditor: false,
    enableFileUpload: false,
    uploadableNumberOfFiles: 1,
    enable: false,
};
