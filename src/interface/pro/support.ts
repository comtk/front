import type {ServerOptions} from 'vue3-easy-data-table';
import {EnumTypeInterface, initEnumType} from "@/interface/pro/enumType";
import {SupportRegisterInterface} from "@/interface/pro/supportRegister";

export interface SupportInterface {
  id?: number | undefined;
  type: EnumTypeInterface | string;
  title: string;
  subTitle: string;
  target: string;
  startDate: string | Date;
  endDate: string | Date;
  contents: string;
  ask: string;
  registerInfo: string;
  supportRegisters: SupportRegisterInterface[];
  images: any[];
  files: any[];
  imageIds: any[];
  fileIds: any[];
  createdDate?: string | Date;
}
export const initSupport: SupportInterface = {
  id: undefined,
  type: 'PRODUCTION',
  title: '',
  subTitle: '',
  target: '',
  startDate: new Date(),
  endDate: new Date(),
  contents: '',
  ask: '',
  registerInfo: '',
  supportRegisters: [],
  images: [],
  files: [],
  imageIds: [],
  fileIds: [],
  createdDate: new Date()
};
