import type {ServerOptions} from 'vue3-easy-data-table';
import {BannerInterface} from '@/interface/pro/banner';

export interface ArticlesInterface {
    id?: number;
    boardsId: number;
    parentArticleId?: number;
    title: string;
    content: string;
    writer: string;
    ip: string;
    answer?: string;
    password?: string;
    link?: string;
    secret?: boolean;
    notice?: boolean;
    deleted?: boolean;
    files?: any[];
    dropzoneFiles?: any[];
    normalFiles?: any[];
}

export interface ArticlesServerOptionsInterface extends ServerOptions {
    boardsId: number;
    pageSize: number;
}

export interface ArticlesFetchInterface {
    boardsId: number;
    articlesId: number;
}

export const initArticles: ArticlesInterface = {
  title: '',
  content: '',
  writer: '',
  ip: '',
  answer: '',
  link: '',
  password: '',
  secret: false,
  notice: false,
  deleted: false,
  boardsId: 0,
  files: [],
  dropzoneFiles: [],
  normalFiles: [],
};
