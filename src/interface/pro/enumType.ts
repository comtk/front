import type {ServerOptions} from 'vue3-easy-data-table';

export interface EnumTypeInterface {
  code: string;
  value: string;
}

export interface EnumTypeServerOptionsInterface extends ServerOptions {
}

export const initEnumType: EnumTypeInterface = {
  code: '',
  value: '',
};
