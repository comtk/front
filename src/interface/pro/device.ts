import type {ServerOptions} from 'vue3-easy-data-table';
import {EnumTypeInterface, initEnumType} from "@/interface/pro/enumType";

export interface DeviceInterface {
  id?: number;
  name: string;
  manufacture: string;
  model: string;
  inYearMonth: string;
  location: string;
  features: string;
  purpose: string;
  cost: string;
  specialNote: string;
  specifications: string;
  url: string;
  enable: boolean;
  buildType: string;
  images: any[];
  imageIds: any[];
  createdDate?: string | Date;
}
export const initDevice: DeviceInterface = {
  name: '',
  manufacture: '',
  model: '',
  inYearMonth: '',
  location: '',
  features: '',
  purpose: '',
  cost: '',
  specialNote: '',
  specifications: '',
  url: '',
  enable: true,
  buildType: 'BUILD',
  images: [],
  imageIds: [],
  createdDate: new Date()
};


