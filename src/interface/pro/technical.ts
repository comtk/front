import type {ServerOptions} from 'vue3-easy-data-table';
import {EnumTypeInterface, initEnumType} from "@/interface/pro/enumType";

export interface TechnicalInterface {
  id?: number | undefined;
  title: string;
  startDate: string | Date;
  endDate: string | Date;
  location: string;
  applyStartDate: string | Date;
  applyEndDate: string | Date;
  capacity: number;
  contents: string;
  files: any[];
  fileIds: any[];
  readCount: number;
  url: string;
  deleted: boolean;
  status: EnumTypeInterface | string;
  createdDate?: string | Date;
}
export const initTechnical: TechnicalInterface = {
  id: undefined,
  title: '',
  startDate: new Date(),
  endDate: new Date(),
  location: '',
  applyStartDate: new Date(),
  applyEndDate: new Date(),
  capacity: 0,
  contents: '',
  files: [],
  fileIds: [],
  readCount: 0,
  url: '',
  deleted: false,
  status: 'BEFORE',
  createdDate: new Date()
};
