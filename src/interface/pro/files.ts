import type {DropzoneFile} from 'vue2-dropzone-vue3';

export interface DropzoneFileInterface extends DropzoneFile {
    fileId: number;
}

export interface DropzoneFilesPropsInterface {
    files: [];
    maxFiles: number;
}

export interface FilesPropsInterface {
    files: [];
    maxFiles: number;
}

export interface FileInterface {
    id: number;
    fileName: string;
    saveFileName: string;
    contentType: string;
    size: number;
    path: string;
    filePath: string;
    hasThumbnail: string;
    thumbnailPath?: string;
    extension: string;
    uploadDate: string;
    fileGroupId?: string;
    storedPath?: string;
    onlyFileName: string;
    absoluteFilePath: string;
}
