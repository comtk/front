export interface DatePickerInterface {
    dateObject: string;
    placeholder: string;
}

export interface DateRangeInterface {
    start: Date;
    end: Date;
}
