export interface UserInterface {
    userId: string;
    name: string;
    email: string;
    phone: string;
    role: string[];
}

export interface UserLoginInterface {
    username: string;
    password: string;
    remember: boolean;
}

export interface UserTokenInterface {
    token: string;
    refreshToken: string;
}
