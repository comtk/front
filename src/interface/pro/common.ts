// import {EquipInterface} from '@/interface/equip';
import {ServerOptions, SortType} from 'vue3-easy-data-table';

export interface ApiErrorInterface {
    message: string;
    errors: ApiErrorsDetailInterface[];
    timestamp: string;
}

export interface ApiErrorsDetailInterface {
    field: string;
    value: string;
    reason: string;
}

export interface ErrorInterface {
    [key: string]: ErrorItemInterface;
}

export interface ErrorItemInterface {
    reason: string;
}

export interface ThemeColorInterface {
    primary: string;
    secondary: string;
    success: string;
    info: string;
    warning: string;
    error: string;
}

export interface SearchInterface {
  searchType: string | undefined;
  keyword: string | undefined;
  page: number;
  rowsPerPage: number;
  sortBy?: string | string[];
  sortType?: SortType | SortType[];
}

export interface SearchDateInterface {
  startDate: string | Date;
  endDate: string | Date;
}

export const initSearchDate: SearchDateInterface = {
  startDate: new Date(),
  endDate: new Date(),
};

export interface SearchArticlesInterface extends SearchInterface {
  boardsId: number;
  searchType: string | undefined;
  keyword: string | undefined;
  page: number;
  rowsPerPage: number;
  sortBy?: string | string[];
  sortType?: SortType | SortType[];
}

export interface AlertInterface {
    type: string;
    variant: string;
    message: string;
    timer: number;
    callback: any;
}

export interface AlertPropsInterface {
    iAlert : AlertInterface;
}
