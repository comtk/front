import type {ServerOptions} from 'vue3-easy-data-table';
import {EnumTypeInterface, initEnumType} from "@/interface/pro/enumType";
import {UserInterface} from "@/interface/pro/user";

export interface MemberInterface extends UserInterface{
  id? : number;
  type: string;
  companyName: string;
  ceo: string;
  companyNumber: string;
  callNumber: string;
  fax: string;
  zipCode: string;
  address: string;
  addressDetail: string;
  deleted: boolean;
  deleteDate: string | Date;
  createdDate : string | Date;
}
export const initMember: MemberInterface = {
  userId: '',
  type: 'NORMAL',
  companyName: '',
  ceo: '',
  companyNumber: '',
  callNumber: '',
  fax: '',
  zipCode: '',
  address: '',
  addressDetail: '',
  deleted: false,
  deleteDate: new Date(),
  createdDate: new Date(),
  name: '',
  email: '',
  phone: '',
  role: [],
};
