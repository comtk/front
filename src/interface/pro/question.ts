import type {ServerOptions} from 'vue3-easy-data-table';
import {EnumTypeInterface, initEnumType} from "@/interface/pro/enumType";

export interface QuestionInterface {
  id?: number | undefined;
  title: string;
  question: string;
  answer: string;
  name: string;
  phone: string;
  email: string;
  status: EnumTypeInterface | string;
  answeredBy: number;
  answeredDate: string | Date;
  createdDate?: string | Date;
}
export const initQuestion: QuestionInterface = {
  id: undefined,
  title: '',
  question: '',
  answer: '',
  name: '',
  phone: '',
  email: '',
  status: 'WAITING',
  answeredBy: 0,
  answeredDate: new Date(),
  createdDate: new Date()
};
