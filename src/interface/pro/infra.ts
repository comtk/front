import type {ServerOptions} from 'vue3-easy-data-table';
import {EnumTypeInterface, initEnumType} from "@/interface/pro/enumType";
import {InfraReservationInterface} from "@/interface/pro/infraReservation";

export interface InfraInterface {
  id?: number;
  name: string;
  capacity: string;
  baseCost: string;
  overCost: string;
  infraInfo: string;
  reservationInfo: string;
  minimumReservationTime: string;
  enable: boolean;
  userDisplay: boolean;
  images: any[];
  imageIds: any[];
  createdDate?: string | Date;
}
export const initInfra: InfraInterface = {
  name: '',
  capacity: '',
  baseCost: '',
  overCost: '',
  infraInfo: '',
  reservationInfo: '',
  minimumReservationTime: '',
  enable: true,
  userDisplay: true,
  images: [],
  imageIds: [],
  createdDate: new Date()
};
