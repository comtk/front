import type {ServerOptions} from 'vue3-easy-data-table';

export interface VisitConnectInterface {
  createdDate?: string | Date;
  visitCnt: number;
}

export interface VisitMediaInterface {
  referer: string;
  visitCnt: number;
}

