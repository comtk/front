import type {ServerOptions} from 'vue3-easy-data-table';

export interface BannerInterface {
  id?: number;
  isUse: boolean;
  sort: number;
  url: string;
  urlTitle: string;
  content: string;
  bannerType: string;
  files: any[];
  createdDate?: string | Date;
}

export interface BannerServerOptionsInterface extends ServerOptions {
}

export const initBanner: BannerInterface = {
  id: 0,
  isUse: true,
  sort: 0,
  url: '',
  urlTitle: '',
  content: '',
  bannerType: 'BANNER',
  files: [],
  createdDate: '',
};
