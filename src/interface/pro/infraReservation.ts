import type {ServerOptions} from 'vue3-easy-data-table';
import {EnumTypeInterface, initEnumType} from "@/interface/pro/enumType";

export interface InfraReservationInterface {
  id?: number;
  infraId: number;
  useStartTime: string | Date;
  useEndTime: string | Date;
  useTime: number;
  eventName: string;
  expectedPerson: number;
  status: string;
  reason: string;
  reservationUser: number;
  createdDate?: string | Date;
}
export const initInfraReservation: InfraReservationInterface = {
  infraId: null,
  useStartTime: new Date(),
  useEndTime: new Date(),
  useTime: 0,
  eventName: '',
  expectedPerson: 0,
  status: 'RESISTER',
  reason: '',
  reservationUser: null,
  createdDate: new Date()
};
