import type {ServerOptions} from 'vue3-easy-data-table';
import {EnumTypeInterface, initEnumType} from "@/interface/pro/enumType";

export interface SupportRegisterInterface {
  id?: number | undefined;
  title: string;
  content: string;
  status: EnumTypeInterface | string;
  reason: string;
  files: any[];
  createdDate?: string | Date;
}
export const initSupportRegister: SupportRegisterInterface = {
  id: undefined,
  title: '',
  content: '',
  status: 'RESISTER',
  reason: '',
  files: [],
  createdDate: new Date()
};
