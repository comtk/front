export interface SupportInterface {
	id?: number;
	supportName: string;
	industry: string;
	startDate: string | Date;
	endDate: string | Date;
	content: string[];
	supportStatus: string;
	target: string;
	files: any[];
	normalFiles: any[];
	contact?: string;
	info?: string[];

}

export const initSupport: SupportInterface = {
	id: 0,
	supportName: "",
	industry: "",
	startDate: new Date(),
	endDate: new Date(),
	content: [],
	supportStatus: "",
	target: "",
	files: [],
	normalFiles: [],
	contact: "",
	info: []
};

export interface SupportSubmitInterface {
	id: number;
	files: any[];
	title: string;
	content?: string;
	type: string;
	status: string;
	support: SupportInterface;
}

export const initSupportSubmit: SupportSubmitInterface = {
	id: 0,
	title: "",
	content: "",
	type: "",
	status: 'ACCEPT',
	support: { ...initSupport },
	files: [],

};

