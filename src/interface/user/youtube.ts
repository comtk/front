export interface YoutubeInterface {
	id: number;
	videoId: string;
	title: string;
	thumbnail: string;
	description: string;
	datetime: string | Date;
	duration: any;
	viewCount: number;
}
export const initYoutube: YoutubeInterface = {
	id: 0,
	videoId: '',
	title: '',
	thumbnail: '',
	description: '',
	datetime: new Date(),
	duration: {},
	viewCount: 0,
};
