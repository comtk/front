export interface PlaceSpecificationsInterface {
	id?: number;
	specificationsName: string;
	content: string;
	sort: number;
}
