import type { PlaceSpecificationsInterface } from '@/interface/user/place/placeSpecifications';

export interface PlaceInterface {
	id?: number;
	placeName: string;
	defaultCost: number;
	defaultCostInfo: string;
	overCost: number;
	overCostInfo: string;
	placeInfo: string;
	max: number;
	place: string;
	status: string;
	files: any[];
}

export interface AdditionalInterface {
	key: string;
	value: string;
}

export interface PlaceSubmitInterface {
	id?: number;
	reserveDate: string | Date | number;
	startTime: string | Date | number;
	useHour: number | string;
	title: string;
	member: number;
	status: string;
	place: PlaceInterface;
}

export const initPlace: PlaceInterface = {
	id: 0,
	placeName: '',
	defaultCost: 0,
	defaultCostInfo: '',
	overCost: 0,
	overCostInfo: '',
	placeInfo: '',
	max: 10,
	place: '',
	status: 'READY',
	files: [],
};
export const initPlaceSubmit: PlaceSubmitInterface = {
	id: 0,
	reserveDate: new Date().setMinutes(0),
	startTime: new Date().setMinutes(0),
	useHour: 4,
	title: '',
	member: 0,
	status: 'ACCEPT',
	place: initPlace
};
