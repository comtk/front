export interface InquiryInterface {
	title: string;
	content: string;
	name: string;
	phone: string;
	email: string;
}

export const initInquiry: InquiryInterface = {
	title: "",
	content: "",
	name: "",
	phone: "",
	email: ""
};