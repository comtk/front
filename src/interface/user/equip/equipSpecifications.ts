export interface EquipSpecificationsInterface {
	id?: number;
	specificationsName: string;
	content: string;
	sort: number;
}
