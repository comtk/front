import type { EquipSpecificationsInterface } from '@/interface/user/equip/equipSpecifications';

export interface EquipInterface {
	id?: number;
	equipName: string;
	model: string;
	place: string;
	buyDate: string | Date;
	content: string;
	equipStatus: string;
	equipSpecifications: EquipSpecificationsInterface[];
	createdDate: string | Date;
	cost: number;
	files: any[];
}

export interface AdditionalInterface {
	key: string;
	value: string;
}

export const initEquip: EquipInterface = {
	id: 0,
	equipName: '',
	model: '',
	place: '',
	buyDate: '',
	content: '',
	equipStatus: '',
	equipSpecifications: [],
	createdDate: '',
	cost: 0,
	files: [],
};
