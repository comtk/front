export interface ApiErrorInterface {
	message: string;
	errors: ApiErrorsDetailInterface[];
	timestamp: string;
}

export interface ApiErrorsDetailInterface {
	field: string;
	value: string;
	reason: string;
}

export interface ErrorInterface {
	[key: string]: ErrorItemInterface;
}

export interface ErrorItemInterface {
	reason: string;
}

export interface ThemeColorInterface {
	primary: string;
	secondary: string;
	success: string;
	info: string;
	warning: string;
	error: string;

	[key: string]: string;
}

export interface ResponseInterface {
	status: number;
	statusText: string;
	data: any;
}

declare global {
	interface Window {
		kakao: any;
	}
}

export interface SearchInterface {
	searchType: string | undefined;
	keyword: string | number | undefined;
	start: number;
	length: number | undefined;
}

export const initSearchData = {
	searchType: '',
	keyword: undefined,
	start: 0,
	length: 10,
};
