import { extend } from "swiper/angular/angular/src/utils/utils";

export interface BoardInterface {
	id: number;
	boardId: number;
	parentArticleId?: number;
	title: string;
	content: string;
	writer: string;
	ip: string;
	answer?: string;
	password?: string;
	link?: string;
	secret?: boolean;
	notice?: boolean;
	deleted?: boolean;
	files: any[];
	dropzoneFiles?: any[];
	normalFiles?: any[];
	readCount: number;
	createdDate: string | Date;
}

export interface PageInterface {
	page: number;
	totalPages: number;
	totalElements: number;
}

export interface PageWithHostInterface extends PageInterface {
	locationHost: number | string;
}

export const initPage: PageInterface = {
	page: 0,
	totalPages: 0,
	totalElements: 0
};

export const initPageWithHost: PageWithHostInterface = {
	page: 0,
	totalPages: 0,
	totalElements: 0,
	locationHost: 0
};


export const initBoard: BoardInterface = {
	createdDate: "",
	readCount: 0,
	id: 0,
	title: "",
	content: "",
	writer: "",
	ip: "",
	answer: "",
	link: "",
	password: "",
	secret: false,
	notice: false,
	deleted: false,
	boardId: 0,
	files: [],
	dropzoneFiles: [],
	normalFiles: []
};

export interface SearchTypeInterface {
	type: string;
	name: string;
}

export const initSearchData = {
	searchType: undefined,
	keyword: undefined,
	start: 0,
	length: 10,
};

export interface VideoInterface extends BoardInterface {
	video: string;
}


/* extends */

export interface EduInterface extends BoardInterface {
	eduStartDate: string | Date;
	eduEndDate: string | Date;
	entryStartDate: string | Date;
	entryEndDate: string | Date;
	max: number;
	place: string;
	status: string;
}

export const initEdu = {
	eduStartDate: new Date(),
	eduEndDate: new Date(),
	entryStartDate: new Date(),
	entryEndDate: new Date(),
	max: 0,
	place: "",
	status: "READY",
	createdDate: new Date(),
	readCount: 0,
	id: 0,
	title: "",
	content: "",
	writer: "",
	ip: "",
	answer: "",
	link: "",
	password: "",
	secret: false,
	notice: false,
	deleted: false,
	boardId: 0,
	files: [],
	dropzoneFiles: [],
	normalFiles: []
};

