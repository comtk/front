export interface MenuInterface {
	path: string;
	title: string;
	subMenu: MenuItemInterface[];
}

export interface MenuItemInterface {
	path: string;
	title: string;
}

export const initMenuItem: MenuItemInterface = {
	path: "/",
	title: ""
};

export const initMenu: MenuInterface = {
	path: "/",
	title: "",
	subMenu: [{ ...initMenuItem }]
};
