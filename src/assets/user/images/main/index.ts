import icArrowPrev from '@/assets/user/images/main/ic-arrow-prev.svg';
import icArrowNext from '@/assets/user/images/main/ic-arrow-next.svg';
import icPause from '@/assets/user/images/main/ic-pause.svg';
import icPlay from '@/assets/user/images/main/ic-play.png';
import icQuick1 from '@/assets/user/images/main/ic-quick-1.svg';
import icQuick2 from '@/assets/user/images/main/ic-quick-2.svg';
import icQuick3 from '@/assets/user/images/main/ic-quick-3.svg';
import icQuick4 from '@/assets/user/images/main/ic-quick-4.svg';
import icPlus from '@/assets/user/images/main/ic-plus.svg';
import icFacebook from '@/assets/user/images/main/ic-facebook.svg';
import bannerSample1 from '@/assets/user/images/main/banner-sample-1.jpg';
import bannerSample2 from '@/assets/user/images/main/banner-sample-1.jpg';
import bannerSample3 from '@/assets/user/images/main/banner-sample-1.jpg';
import bannerSample4 from '@/assets/user/images/main/banner-sample-1.jpg';
import swiperSample from '@/assets/user/images/main/swiper-sample.png';

import icSwiperPause from '@/assets/user/images/main/ic-swiper-pause.svg';
import icSwiperNext from '@/assets/user/images/main/ic-swiper-next.svg';
import icSwiperPrev from '@/assets/user/images/main/ic-swiper-prev.svg';


import logoSample1 from '@/assets/user/images/main/logo-sample-1.png';
import logoSample2 from '@/assets/user/images/main/logo-sample-2.png';
import logoSample3 from '@/assets/user/images/main/logo-sample-3.png';
import logoSample4 from '@/assets/user/images/main/logo-sample-4.png';
import logoSample5 from '@/assets/user/images/main/logo-sample-5.png';


import icSearchMain from '@/assets/user/images/main/ic-search-main.svg';

import icPauseDark from '@/assets/user/images/main/ic-pause-dark.svg';
import icPlayDark from '@/assets/user/images/main/ic-play-dark.png';

export {
	icArrowPrev,
	icArrowNext,
	icPause,
	icPlay,
	icQuick1,
	icQuick2,
	icQuick3,
	icQuick4,
	icPlus,
	icFacebook,
	bannerSample1,
	bannerSample2,
	bannerSample3,
	bannerSample4,
	swiperSample,
	icSwiperPause,
	icSwiperNext,
	icSwiperPrev,
	logoSample1,
	logoSample2,
	logoSample3,
	logoSample4,
	logoSample5,
	icSearchMain,
	icPauseDark,
	icPlayDark,
};
