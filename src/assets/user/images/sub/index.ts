import icNavArrow from '@/assets/user/images/sub/common/ic-nav-arrow.svg';
import icHome from '@/assets/user/images/sub/common/ic-home.svg';
import icCheck from '@/assets/user/images/sub/common/ic-check.svg';

import corporationIntro from '@/assets/user/images/sub/corporation/corporation-intro.png';
import corporationCorporationPC1 from '@/assets/user/images/sub/corporation/corporation-corporation-pc-1.png';
import corporationCorporationMobile1 from '@/assets/user/images/sub/corporation/corporation-corporation-mobile-1.png';
import corporationCorporationPC2 from '@/assets/user/images/sub/corporation/corporation-corporation-pc-2.png';
import corporationCorporationMobile2 from '@/assets/user/images/sub/corporation/corporation-corporation-mobile-2.png';
import corporationOrganPC1 from '@/assets/user/images/sub/corporation/corporation-organ-pc-1.png';
import corporationOrganMobile1 from '@/assets/user/images/sub/corporation/corporation-organ-mobile-1.png';
import corporationIntroName from '@/assets/user/images/sub/corporation/corporation-intro-name.png';
import icPin from '@/assets/user/images/sub/corporation/ic-pin.svg';
import icPhone from '@/assets/user/images/sub/corporation/ic-phone.svg';

import businessSocialPC1 from '@/assets/user/images/sub/business/business-social-pc-1.png';
import businessSocialMobile1 from '@/assets/user/images/sub/business/business-social-mobile-1.png';
import businessSocialPC2 from '@/assets/user/images/sub/business/business-social-pc-2.png';
import businessSocialMobile2 from '@/assets/user/images/sub/business/business-social-mobile-2.png';
import businessUnionPC1 from '@/assets/user/images/sub/business/business-union-pc-1.png';
import businessUnionMobile1 from '@/assets/user/images/sub/business/business-union-mobile-1.png';
import businessUnionPC2 from '@/assets/user/images/sub/business/business-union-pc-2.png';
import businessUnionMobile2 from '@/assets/user/images/sub/business/business-union-mobile-2.png';
import businessUnionPC3 from '@/assets/user/images/sub/business/business-union-pc-3.png';
import businessUnionMobile3 from '@/assets/user/images/sub/business/business-union-mobile-3.png';
import icBusinessUnion1 from '@/assets/user/images/sub/business/ic-business-union-1.svg';
import icBusinessUnion2 from '@/assets/user/images/sub/business/ic-business-union-2.svg';
import icBusinessUnion3 from '@/assets/user/images/sub/business/ic-business-union-3.svg';
import businessPromotePC1 from '@/assets/user/images/sub/business/business-promote-pc-1.png';
import businessPromoteMobile1 from '@/assets/user/images/sub/business/business-promote-mobile-1.png';
import businessSupportPC1 from '@/assets/user/images/sub/business/business-support-pc-1.png';
import businessSupportMobile1 from '@/assets/user/images/sub/business/business-support-mobile-1.png';
import businessSupportPC2 from '@/assets/user/images/sub/business/business-support-pc-2.png';
import businessSupportMobile2 from '@/assets/user/images/sub/business/business-support-mobile-2.png';
import businessSupportPC3 from '@/assets/user/images/sub/business/business-support-pc-3.png';
import businessSupportMobile3 from '@/assets/user/images/sub/business/business-support-mobile-3.png';
import businessSupportPC4 from '@/assets/user/images/sub/business/business-support-pc-4.png';
import businessSupportMobile4 from '@/assets/user/images/sub/business/business-support-mobile-4.png';


import icPageFirst from '@/assets/user/images/sub/board/ic-page-first.svg';
import icPagePrev from '@/assets/user/images/sub/board/ic-page-prev.svg';
import icPageNext from '@/assets/user/images/sub/board/ic-page-next.svg';
import icPageLast from '@/assets/user/images/sub/board/ic-page-last.svg';
import icSearchBlue from '@/assets/user/images/sub/board/ic-search-blue.svg';
import icBoardPrev from '@/assets/user/images/sub/board/ic-board-prev.svg';
import icBoardNext from '@/assets/user/images/sub/board/ic-board-next.svg';
import icAttach from '@/assets/user/images/sub/board/ic-attach.svg';
import icEye from '@/assets/user/images/sub/board/ic-eye.svg';

import sponLogo from '@/assets/user/images/sub/spon/spon-logo.png';



export {
	icNavArrow,
	icHome,
	icCheck,
	corporationIntro,
	corporationCorporationPC1,
	corporationCorporationMobile1,
	corporationCorporationPC2,
	corporationCorporationMobile2,
	corporationOrganPC1,
	corporationOrganMobile1,
	corporationIntroName,
	icPin,
	icPhone,
	businessSocialPC1,
	businessSocialMobile1,
	businessSocialPC2,
	businessSocialMobile2,
	businessUnionPC1,
	businessUnionMobile1,
	businessUnionPC2,
	businessUnionMobile2,
	businessUnionPC3,
	businessUnionMobile3,
	icBusinessUnion1,
	icBusinessUnion2,
	icBusinessUnion3,
	businessPromotePC1,
	businessPromoteMobile1,
	businessSupportPC1,
	businessSupportMobile1,
	businessSupportPC2,
	businessSupportMobile2,
	businessSupportPC3,
	businessSupportMobile3,
	businessSupportPC4,
	businessSupportMobile4,
	icPageFirst,
	icPagePrev,
	icPageNext,
	icPageLast,
	icSearchBlue,
	icBoardPrev,
	icBoardNext,
	icAttach,
	icEye,
	sponLogo,
}