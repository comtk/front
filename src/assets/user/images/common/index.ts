import imgDefault from '@/assets/user/images/common/img-default.svg';

import logo from '@/assets/user/images/common/logo.png';
import footerLogo from '@/assets/user/images/common/footer-logo.png';
import icMoveTop from '@/assets/user/images/common/ic-move-top.svg';
import icMenu from '@/assets/user/images/common/ic-menu.svg';
import icSearchBlack from '@/assets/user/images/common/ic-search-black.svg';
import imgNoImage from '@/assets/user/images/common/img-no-image.svg';

export { logo, footerLogo, icMoveTop, icMenu, imgDefault, icSearchBlack, imgNoImage };
