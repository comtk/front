import { defineStore } from 'pinia';
import { computed, ref } from 'vue';
import type { MenuInterface } from '@/interface/user/menu';
import { initMenu } from '@/interface/user/menu';

export const menuStore = defineStore('menu', () => {
	const menu = ref<MenuInterface[]>([{ ...initMenu }]);

	function handleMenu(menuData: MenuInterface[]) {
		menu.value = menuData;
	}

	const getMenu = computed(() => menu.value);

	return {
		menu,
		handleMenu,
		getMenu,
	};
});
