import {defineStore} from 'pinia';
import {computed, ref} from 'vue';
import type {UserInterface, UserTokenInterface} from '@/interface/pro/user';
import {AlertInterface} from "@/interface/pro/common";

export const userStore = defineStore('user', () => {
    // pinia에 저장되는 객체
    const currentUser = ref<UserInterface>();

    // pinia에 저장되어있는 객체의 값을 변경할수있는 함수.
    function handleCurrentUser(accountInfo: UserInterface | undefined) {
        currentUser.value = accountInfo;
    }

    // pinia에 있는 값을 가져오는 함수
    const getCurrentUser = computed(() => currentUser.value);

    const currentToken = ref<UserTokenInterface>();

    function handleCurrentToken(tokenObject: UserTokenInterface | undefined) {
        currentToken.value = tokenObject;
    }

    const getCurrentToken = computed(() => currentToken.value);

    const iAlert = ref<AlertInterface>({
      type: 'success',
      variant: 'flat',
      message: '',
      timer: 1500,
      callback: () => {}
    });

    // 객체 및 함수를 return 해줘야 정상적으로 사용 및 확인 가능. 확인은 크롬 개발자도구 > vue > pinia
    return {
        currentUser,
        handleCurrentUser,
        getCurrentUser,
        currentToken,
        handleCurrentToken,
        getCurrentToken,
        iAlert,
    };
});
