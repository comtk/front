import { setupLayouts } from "virtual:generated-layouts";
import type { RouteRecordRaw } from "vue-router";
import { createRouter, createWebHistory } from "vue-router";
import routes from "~pages";

// 로그인이 필요한 페이지 패턴 적용.
const authPathRegex = new RegExp(/^\/my.*$/);
const authPathList = [
	'/resource/place/:id/form',
	'/support/production/:id/form',
	'/support/evaluation/:id/form',
	'/support/consulting/:id/form',
	'/pro',
];
const authSetRoutes = (): RouteRecordRaw[] => {
	const unsetRoutes = setupLayouts(routes);
	unsetRoutes.forEach(routeObject => {
		routeObject.meta = {
			authRequired: authPathRegex.test(routeObject.path) || authPathList.includes(routeObject.path),
		};
		// 모든페이지 로그인없이 접근 가능.
		// routeObject.meta = { authRequired: false };
	});

	return unsetRoutes;
};

const router = createRouter({
	history: createWebHistory(import.meta.env.BASE_URL),
	routes: [...authSetRoutes()],
	scrollBehavior() {
		return { top: 0 };
	}
});

router.beforeEach(async (to, from, next) => {

	if (
		to.matched.some(routeInfo => {
			return routeInfo.meta.authRequired;
		})
	) {
		next();
	} else {
		next();
	}
});

router.afterEach((to, from) => {
	localStorage.setItem("lastPage", to.path);
});

export default router;
