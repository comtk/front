import {useTheme} from 'vuetify';
import moment from 'moment/moment';
import {computed} from 'vue';
import type {ApiErrorInterface, ErrorInterface, ThemeColorInterface} from '@/interface/user/common';
import {userStore} from "@/store/pro/user";

// 👉 IsEmpty
export const isEmpty = (value: unknown): boolean => {
	if (value === null || value === undefined || value === '') {
		return true;
	}

	return Array.isArray(value) && value.length === 0;
};

// 👉 IsNullOrUndefined
export const isNullOrUndefined = (value: unknown): value is undefined | null => {
	return value === null || value === undefined;
};

// 👉 IsEmptyArray
export const isEmptyArray = (arr: unknown): boolean => {
	return Array.isArray(arr) && arr.length === 0;
};

// 👉 IsObject
export const isObject = (obj: unknown): obj is Record<string, unknown> =>
	obj !== null && !!obj && typeof obj === 'object' && !Array.isArray(obj);

// 👉 get vuetify theme main Colors
export const getVuetifyThemeColors = (): ThemeColorInterface => {
	const theme = useTheme();

	return theme.current.value.colors;
};

// 👉 get vuetify theme name
export const getVuetifyCurrentTheme = (): any => {
	const vuetifyTheme = useTheme();

	return computed(() => {
		return vuetifyTheme.global.name.value;
	});
};

// 👉 change date format
export const changeNumberFormat = (number: number | string | undefined) => {
	if (!isEmpty(number) && number !== undefined) {
		const locale = navigator.language;

		if (typeof number === 'string') {
			number = Number(number);
		}

		return new Intl.NumberFormat(locale).format(number);
	}
};

// 👉 api post error convertor
export const convertErrorObject = (errorResponse: ApiErrorInterface): ErrorInterface => {
	const errObj: any = {};
	errorResponse.errors.forEach(err => {
		errObj[err.field] = {
			reason: err.reason,
		};
	});

	return errObj;
};

export const textMask = (text: string) => {
	let result = text;
	if (!isEmpty(text)) {
		result = text.substring(0, 1);
		const length = text.length;
		if (length > 0) {
			for (let step = 0; step < length - 1; step++) {
				result = `${result}●`;
			}
		}
	}

	return result;
};

// 👉 change date format
// https://developer.mozilla.org/ko/docs/Web/JavaScript/Reference/Global_Objects/Intl/DateTimeFormat/DateTimeFormat

export const changeDateFormat = (date: string | Date | number | undefined) => {
	if (!isEmpty(date) && date) {
		const convertDate = new Date(date);
		const locale = navigator.language;

		return new Intl.DateTimeFormat(locale, {year: 'numeric', month: '2-digit', day: '2-digit'}).format(
			convertDate,
		);
	}
};

export const changeDateFormatWithWeekday = (date: string | Date | number | undefined) => {
	if (!isEmpty(date) && date) {
		const convertDate = new Date(date);
		const locale = navigator.language;

		return new Intl.DateTimeFormat(locale, {
			year: 'numeric',
			month: '2-digit',
			day: '2-digit',
			weekday: 'long',
		}).format(convertDate);
	}
};

// 👉 change date format
export const changeDateFormatMoment = (date: string | Date | number, format: string = 'YYYY-MM-DD'): string => {
	const convertDate = new Date(date);

	return moment(convertDate).format(format);
};

export const timeForToday = (value: string | Date | number) => {
	const today = new Date();
	const timeValue = new Date(value);

	const betweenTime = Math.floor((today.getTime() - timeValue.getTime()) / 1000 / 60);
	if (betweenTime < 1) return '방금전';
	if (betweenTime < 60) {
		return `${betweenTime}분전`;
	}

	const betweenTimeHour = Math.floor(betweenTime / 60);
	if (betweenTimeHour < 24) {
		return `${betweenTimeHour}시간전`;
	}

	const betweenTimeDay = Math.floor(betweenTime / 60 / 24);
	if (betweenTimeDay < 365) {
		return `${betweenTimeDay}일전`;
	}

	return `${Math.floor(betweenTimeDay / 365)}년전`;
}

// 👉 get D-DAY count
export const dDay = (value: string | Date | number) => {
	const targetDate = new Date(value);
	const today = new Date();
	const diffDate = targetDate.getTime() - today.getTime();

	const diffDay = Math.round(Math.abs(diffDate / (1000 * 60 * 60 * 24)));
	return diffDay;
}

// 👉 get random color
export const randomColor = () => Math.floor(Math.random()*16777215).toString(16);

// 👉 get api url
export const getApiHost = () => {
	return import.meta.env.VITE_API_BASE_URL;
};

export const getApiUrl = () => {
	return '/user/api/v1';
};

export const getApiPath = (path: string) => {
	return `${getApiHost()}${getApiUrl()}${path}`;
};

// 👉 get file url
export const getFileUrl = () => {
	return getApiPath('/files');
};

// 👉 set default image
export const setErrorImage = (obj: any) => {
	const keys = Object.getOwnPropertyNames(obj);
	keys.forEach((key) => {
		if(key === 'imageId' || key === 'imageIds' || key === 'imagesIds'){
			if(typeof obj[key] === 'string'){
				obj[key] = '';
			}else{
				obj[key] = [];
			}
		}
	})
}

// 👉 add blank item
export const addTempItem = (list: any, end: number) => {
	if (end > 0) {
		let i = 0;
		for (i = 0; i < end; i++) {
			const temp = {
				id: -1,
				title: '',
				createdDate: '',
				link: '/',
			};
			list.value.push(temp);
		}
	}
};

// 👉 check login
// export const checkLogin = async () => {
// 	const token = localStorage.getItem("token");
// 	if (token) {
// 		return await apiGetUserInfo();
// 	} else {
// 		return false;
// 	}
// };




// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

export const alertMessage = (type : string, message : string,  timer : number, callback? : any) => {
	const store = userStore();

	store.iAlert.type = type;
	store.iAlert.message = message;
	store.iAlert.callback = isEmpty(callback) ? () => {} : callback;
	store.iAlert.timer = isEmpty(timer) ? 1500 : timer;
}

export const alertSuccess = (message : string, callback? : any, timer? : number) => {
	alertMessage('success', message, callback, timer);
}

export const alertInfo = (message : string, callback? : any, timer? : number) => {
	alertMessage('info', message, callback, timer);
}

export const alertWarning = (message : string, callback? : any, timer? : number) => {
	alertMessage('warning', message, callback, timer);
}

export const alertError = (message : string, callback? : any, timer? : number) => {
	alertMessage('error', message, callback, timer);
}

